# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Clientes(models.Model):
    id_cliente = models.BigAutoField(primary_key=True)
    cedula = models.CharField(max_length=100)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)

    def __str__(self):
        return self.cedula
    class Meta:
        db_table = 'clientes'

class Servicios(models.Model):
    id_servicio = models.BigAutoField(primary_key=True)
    servicio_descripcion = models.CharField(max_length=40)
    id_cargo = models.BigAutoField(Cargos, on_delete=models.CASCADE)

    def __str__(self):
        return self.servicio_descripcion
    class Meta:
        db_table = 'servicios'

class Prioridades(models.Model):
    id_prioridad = models.BigAutoField(primary_key=True)
    prioridad_descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.prioridad_descripcion
    class Meta:
        db_table = 'prioridades'

class Cargos(models.Model):
    id_cargo = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion 
    class Meta:
        db_table = 'Cargos'

class Estado(models.Model):
    estado = models.BigAutoField(primary_key=True)
    estado_descripcion = models.CharField(max_length=30)

    def __str__(self):
        return self.descripcion
    class Meta:
        db_table = 'estado'

class Permisos(models.Model):
    id_permiso = models.BigAutoField(primary_key=True)
    permiso_descripcion = models.CharField(max_length=50)

    def __str__(self):
        return self.descripcion
    class Meta:
        db_table = 'permisos'

class Empleado(models.Model):
    id_empleado = models.BigAutoField(primary_key=True)
    nombre_empleado = models.CharField(max_length=40)
    apellido_empleado = models.CharField(max_length=40)
    empleado_cedula = models.CharField(max_length=20)
    id_cargo = models.ForeignKey(Cargos, on_delete=models.CASCADE)
    rol = models.ForeignKey('Roles', on_delete=models.CASCADE, db_column='rol')

    def __str__(self):
        return self.nombre_empleado
    class Meta:
        db_table = 'empleado'

class Roles(models.Model):
    rol = models.BigAutoField(primary_key=True)
    rol_descripcion = models.CharField(max_length=50)
    id_permiso = models.BigAutoField(Permisos, on_delete=models.CASCADE, db_column='id_permiso')

    def __str__(self):
        return self.rol_descripcion
    class Meta:
        db_table = 'roles'

class Ticket(models.Model):
    id_ticket = models.BigAutoField(primary_key=True)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE, db_column='estado')
    id_cliente = models.BigAutoField(Clientes, on_delete=models.CASCADE, db_column='id_cliente')

    def __str__(self):
        return self.estado
    class Meta:
        db_table = 'ticket'

class ColaEspera(models.Model):
    id_cola_trabajo = models.BigAutoField(primary_key=True)
    id_ticket = models.ForeignKey('Ticket', on_delete=models.CASCADE, db_column='id_ticket')
    id_prioridad = models.ForeignKey('Prioridades', on_delete=models.CASCADE, db_column='id_prioridad')
    id_servicio = models.ForeignKey('Servicios', on_delete=models.CASCADE, db_column='id_servicio')
    fecha_creacion = models.DateTimeField()
    fecha_actualizacion = models.DateTimeField()

    def __str__(self):
        return self.id_ticket
    class Meta:
        db_table = 'cola_espera'